<?php
class CiviCrmSearchApiDeletedFilter extends SearchApiAbstractAlterCallback {

  /**
   * {@inheritdoc}
   */
  public function supportsIndex(SearchApiIndex $index) {
    return $this->isCiviContactIndex($index);
  }

  /**
   * {@inheritdoc}
   */
  public function alterItems(array &$items) {
    // Make sure we're running on the supported type of indexes.
    if (!$this->supportsIndex($this->index)) {
      return;
    }


    foreach ($items as $id => $item) {
      // If $item[$id]->deleted, then remove.
      if(self::isContactDeleted($id)) {
        unset($items[$id]);
      }
    }
  }

  /**
   * Checks if a CiviCRM contact is deleted.
   * @param int $contact_id
   * @return bool True if deleted, false if not.
   */
  private static function isContactDeleted($contact_id) {
    // We have to hit the database because civicrm_entity doesn't
    // include the is_deleted property with the entity.

    // For performance, we'll go straight to the DB
    // instead of using the CiviCRM API.
    $deleted = db_select('civicrm_contact', 'c')
      ->fields('c', array('is_deleted'))
      ->condition('c.id', $contact_id)
      ->execute()
      ->fetchCol();
    $deleted = reset($deleted);
    return !empty($deleted);
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm() {
    return parent::configurationForm();
  }

  /**
   * {@inheritdoc}
   */
  public function configurationFormSubmit(array $form, array &$values, array &$form_state) {
    return parent::configurationFormSubmit($form, $values, $form_state);
  }


  /**
   * Detects whether the $index is based on a CiviCRM Contact entity.
   *
   * @param \SearchApiIndex|NULL $index
   * @return bool
   */
  protected function isCiviContactIndex(SearchApiIndex $index = null) {
    $data_source = $index->datasource();
    return $data_source->getEntityType() == 'civicrm_contact';
  }
}
